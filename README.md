Ce [site] est un exemple de mise en page avec **Flexbox**, dans le cadre de
l'acquisition du HTML5 et CSS3.

Cette démonstration, librement inspiré du [tuto] d'open class rooms,
vise à répartir des cadres de couleur sur plusieurs lignes.

L'objectif est également de déployer la page statique sur les "frama
pages" à l'aide du moteur d'intégration continue de Gitlab.

[tuto]: https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/3298561-la-mise-en-page-avec-flexbox
[site]: http://s2sio.frama.io/web/flexbox_3-multi-lignes
